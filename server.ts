import express, { Request, Response } from 'express';
const server = express();

server.all('/', (req: Request, res: Response) => {
  res.send('Bot is running');
});  
  
export const openServer = () => {
  server.listen(process.env.PORT || 3000, () => {
    console.log('Server is running');
  });
};